class BreedsController < ApplicationController

  def index
    @breed =  params[:breed].present? ?  DogBreedFetcher.new(params[:breed]).fetch  :  DogBreedFetcher.fetch
  end

end
